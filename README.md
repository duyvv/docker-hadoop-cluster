# SS AACM Docker Hadoop Repository

**This repository consists of different branches which were created for various purposes on studying Docker and Hadoop.**

## 1. The branch: 'docker-basic'

- This branch (docker-basic) aims to study fundamental Docker and examplify a simple Docker network where its Docker containers run simple Flask apps. The Docker network enables accessing to the Flask apps across the Docker containers as well.

- Details available at https://gitlab.com/duyvv/docker-hadoop-cluster/-/tree/docker-basic?ref_type=heads

## 2. The branch 'docker-hadoop-cluster-v1.0'

- This branch (docker-hadoop-cluster-v1.0) aims to study basic Hadoop framework and experiment a multi-node hadoop cluster (1 namenode + 2 datanodes) using Docker containers

- Details available at https://gitlab.com/duyvv/docker-hadoop-cluster/-/tree/docker-hadoop-cluster-v1.0?ref_type=heads

